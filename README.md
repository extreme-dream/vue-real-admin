#  vue-real-admin 

一个多窗口后台模板，流畅、易上手、提高生产力
 

:kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes:  :blush:  :blush:  :blush:  :heart:  :heart:  :heart: 

## 优点
- 上手简单：目录结构简单、0封装，代码与您坦诚相见
- 可扩展性：可扩展性强、几乎0封装的代码，实现了基础的后台操作；方便二次封装
- 示例全面：提供大量常见增删改查示例，提高你的生产力

## 需求提交
- 我们深知一个优秀的项目需要海纳百川，[点我在线提交需求](https://gitee.com/extreme-dream/vue-real-admin/issues)

## 演示地址
[http://vue-real-admin.jmwl51.com/](http://vue-real-admin.jmwl51.com/)

## 依赖
axios、default-passive-events、 echarts、element-plus、vue、vue-router、vuex


## 开始使用
 
#### 安装依赖


```
npm i
```
#### 运行

```
npm rum dev
```
#### 打包

```
npm rum build
```


## 贡献代码
1. 在github上fork一份到自己的仓库
2. clone自己的仓库到本地电脑
3. 在本地电脑修改、commit、push
4. 提交pr（点击：New Pull Request）（提交pr前请保证自己fork的仓库是最新版本，如若不是先强制更新一下）
5. 等待合并

## 建议贡献的地方
- 更多登录模板
- 修复源码现有bug，或增加新的实用功能（比如：流畅的tab左右拖拽排序）
- 更多demo示例：比如针对element-ui一些复杂组件的示例，或者其它一些常见js库的集成使用
- 如果更新实用功能，可在文档友情链接处留下自己的推广链接
 
## 软件架构

```

  src  应用部署目录
  ├─api                                                接口
  ├─assets                                             公共文件
  ├─components                                         
  │  ├─ActionButton. vue                               列表表格头部按钮组件（未完成）
  │  ├─BreadCrumb. vue                                 框架面包屑组件
  │  ├─Home. vue                                       主框架组件（引用面包屑、菜单列表）
  │  ├─JmTable. vue                                    列表页表格组件（引用表格tb）
  │  ├─JmTb. vue                                       表格单元tb组件 
  │  ├─QueryForm. vue                                  表格头部搜索组件（未完成）
  │  ├─TreeMenu. vue                                   菜单列表组件 
  │  ├─form                                            
  │  │  ├─JmInput. vue                                 Input文件
  │  │  ├─JmEnum. vue                                  选择框文件（未完成）
  │  │  ├─list. vue                                    列表文件（未完成）
  ├─config                                             配置
  ├─router                                             路由管理
  ├─store                                              
  │  ├─index.js                                        用户状态文件
  │  ├─mutations.js                                    业务层数据提交
  ├─utils                                             
  │  ├─request.js                                      axios二次封装文件
  │  ├─request.js                                      Storage二次封装
  ├─views                                             
  │  ├─auth                                             
  │  │  ├─rule                                        
  │  │  │  ├─index. vue                                列表页视图
  │  │  │  ├─index.js                                  列表页js
  │  │  │  ├─add. vue                                  添加页视图
  │  │  │  ├─edit. vue                                 修改页视图
  │  └─ ...                                         更多类库目录
 
```
## 软件视图效果
 
 ![输入图片说明](public/image1.png)
![输入图片说明](./public/image.png)
![输入图片说明](./public/image3.png)
![输入图片说明](./public/image4.png)
## 软件账号密码
默认账号：admin
默认密码：123456

## 接口文档

[https://www.eolink.com/share/index?shareCode=7mEAG4](https://www.eolink.com/share/index?shareCode=7mEAG4)

## 组件
#### 表格组件

```

    dataList:  [],
    columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   },  
                    {label: "名称",        prop: "name",    }, 
                    {label: "icon",        prop: "icon",  type:"icon"   }, 
                    {label: "avatar",        prop: "avatar",    type:"avatar"   }, 
                    {label: "count",        prop: "count",   type:"tc-num"   }, 
                    {label: "money",        prop: "money",   type:"money"   }, 
                    {label: "money",        prop: "money",   type:"money-f"   }, 
                    {label: "rate",        prop: "rate",   type:"rate"   },  
                    {label: "intro",        prop: "intro",   type:"textarea"   }, 
                    {label: "intro",        prop: "intro",   type:"rich-text"   }, 
                    {label: "avatar",        prop: "avatar",   type:"link"   },  
                    {label: "avatar",        prop: "avatar",   type:"img"   }, 
                    {label: "create_time",        prop: "create_time",   type:"date"   }, 
                    {label: "datetime",        prop: "create_time",   type:"datetime"   },  
                    
                ],//行参数
```
 **组件支持方法** 

- add
- edit
- del
 **自定义方法** 
1、打开JmTable组件，添加事件
2、通过 this.$parent.del(_id);调用父页面方法
#### 表单组件
               
```
                    <JmInput jmname="普通输入：" v-model="form.name"></JmInput>
                    <JmInput type="num" jmname="数字输入：" v-model="form.age"></JmInput>
                    <JmInput type="password" jmname="密码输入：" v-model="form.password"></JmInput>
                    <JmInput type="textarea" jmname="多行输入：" v-model="form.name"></JmInput>
                    <JmInput type="date" jmname="日期输入：" v-model="form.createTime"></JmInput>
                    <JmInput type="datetime" jmname="日期时间：" v-model="form.createTime2"></JmInput>
                    <JmInput type="time" jmname="时间输入：" v-model="form.createTime3"></JmInput>
                    <in-list type="text-list" jmname="文本列表：" v-model="form.urlList"></in-list>
                    <JmInput type="slider" jmname="滑块参数：" v-model="form.widthValue"></JmInput>
                    <JmInput type="color" jmname="颜色选择：" v-model="form.color"></JmInput>
                    <JmInput type="rate" jmname="评分组件：" v-model="form.rate"></JmInput>
                    <JmInput type="money" jmname="金额输入：" v-model="form.moneyFen"></JmInput> 
                  
```
`图片`等，直接用element-plus自带的

例：
```

            <el-form-item label="头像">
              <el-upload
                class="avatar-uploader"
                action="http://real-think.jmwl51.com//admin/Ajaxs/upload"
                :show-file-list="false"
                :on-success="handleAvatarSuccess"
                :before-upload="beforeAvatarUpload"
              >
                <img v-if="form.avatar" :src="form.avatar" class="avatar" />
                <el-icon v-else class="avatar-uploader-icon"><Plus /></el-icon>
              </el-upload>
            </el-form-item>
```
## 接口调用方式
 
`this.$api.getRule(); `