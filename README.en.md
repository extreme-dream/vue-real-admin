# vue-real-admin



A multi window background template, smooth, easy to use and improve productivity

 

:kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes: :kissing_closed_eyes:  :kissing_closed_eyes:  :kissing_closed_eyes:  :blush:  :blush:  :blush:  :heart:  :heart:  :heart: 


## Advantages

- Easy to use: the directory structure is simple, 0 encapsulated, and the code is honest with you

- Extensibility: the code with strong extensibility and almost 0 encapsulation realizes the basic background operation; Convenient secondary packaging

- Comprehensive examples: provide a large number of common examples of addition, deletion, modification and query to improve your productivity



## Demand submission

- We know that an excellent project needs to accept all rivers, [click me to submit the demand online]（ https://gitee.com/extreme-dream/vue-real-admin/issues )




## Rely on

axios、default-passive-events、 echarts、element-plus、vue、vue-router、vuex




## Start using



#### Installation dependency




```

npm i

```

#### Run



```

npm rum dev

```

#### Pack



```

npm rum build

```




## Contribution code

1. Fork a copy on GitHub to your own warehouse

2. Clone own warehouse to local computer

3. Modify, commit and push on the local computer

4. Submit pr (click: new pull request) (before submitting PR, please ensure that your fork warehouse is the latest version, if not forced to update first)

5. Pending consolidation



## Suggested contribution

- More login templates

- Fix existing bugs in the source code, or add new practical functions (such as smooth tab left-right drag sorting)

- More demo examples: for example, for some complex components of element UI, or the integrated use of some other common JS libraries

- If you update the practical function, you can leave your own promotion link at the friendly link of the document



## Software architecture



```



SRC application deployment directory

Interface - API interface

Keywords - assets public file

├─components

│ ├─ActionButton. Vue list table header button assembly (incomplete)

│ ├─BreadCrumb. Vue frame crumb assembly

│ ├─Home. Vue main frame components (reference bread crumbs, menu list)

│ ├─JmTable. Vue list page table component (reference table tb)

│ ├─JmTb. Vue table cell TB component

│ ├─QueryForm. Vue table header search component (incomplete)

│ ├─TreeMenu. Vue menu list component

│ ├─form

│ │ ├─JmInput. Vue input file

│ │ ├─JmEnum. Vue selection box file (incomplete)

│ │ ├─list. Vue list file (incomplete)

Configuration - config configuration

Network router routing management

├─store

│ ├─index. JS user status file

│ ├─mutations. JS business layer data submission

├─utils

│ ├─request. JS Axios secondary encapsulation file

│ ├─request. JS storage secondary packaging

├─views

│ ├─auth

│ │ ├─rule

│ │ │ ├─index. Vue list page view

│ │ │ ├─index. JS list page JS

│ │ │ ├─add. Vue add page view

│ │ │ ├─edit. Vue modify page view

│ └─ ... More class library directories



```

## Software view effect



! [enter picture. PNG] / 1

! [enter picture description] (. / public / image. PNG)

! [enter picture description] (. / public / image3. PNG)

! [enter picture description] (. / public / image4. PNG)

## Software account password

Default account: admin

Default password: 123456



## Interface document



[ https://www.eolink.com/share/index?shareCode=7mEAG4 ]( https://www.eolink.com/share/index?shareCode=7mEAG4 )



## Components

#### Table component



```



    dataList:  [],
    columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   },  
                    {label: "名称",        prop: "name",    }, 
                    {label: "icon",        prop: "icon",  type:"icon"   }, 
                    {label: "avatar",        prop: "avatar",    type:"avatar"   }, 
                    {label: "count",        prop: "count",   type:"tc-num"   }, 
                    {label: "money",        prop: "money",   type:"money"   }, 
                    {label: "money",        prop: "money",   type:"money-f"   }, 
                    {label: "rate",        prop: "rate",   type:"rate"   },  
                    {label: "intro",        prop: "intro",   type:"textarea"   }, 
                    {label: "intro",        prop: "intro",   type:"rich-text"   }, 
                    {label: "avatar",        prop: "avatar",   type:"link"   },  
                    {label: "avatar",        prop: "avatar",   type:"img"   }, 
                    {label: "create_time",        prop: "create_time",   type:"date"   }, 
                    {label: "datetime",        prop: "create_time",   type:"datetime"   },  
                    
                ],//line parameters

```

** Component support method**



- add

- edit

- del

**Custom method**

1. Open the jmtable component and add an event

2. Through this$ parent. del(_id); Call parent page method

#### Form component


``` 
     <JmInput jmname="普通输入：" v-model="form.name"></JmInput>
                    <JmInput type="num" jmname="数字输入：" v-model="form.age"></JmInput>
                    <JmInput type="password" jmname="密码输入：" v-model="form.password"></JmInput>
                    <JmInput type="textarea" jmname="多行输入：" v-model="form.name"></JmInput>
                    <JmInput type="date" jmname="日期输入：" v-model="form.createTime"></JmInput>
                    <JmInput type="datetime" jmname="日期时间：" v-model="form.createTime2"></JmInput>
                    <JmInput type="time" jmname="时间输入：" v-model="form.createTime3"></JmInput>
                    <in-list type="text-list" jmname="文本列表：" v-model="form.urlList"></in-list>
                    <JmInput type="slider" jmname="滑块参数：" v-model="form.widthValue"></JmInput>
                    <JmInput type="color" jmname="颜色选择：" v-model="form.color"></JmInput>
                    <JmInput type="rate" jmname="评分组件：" v-model="form.rate"></JmInput>
                    <JmInput type="money" jmname="金额输入：" v-model="form.moneyFen"></JmInput> 

```

`Pictures, etc. directly use the self-contained image of element plus



Example:

```

   <el-form-item label="头像">
              <el-upload
                class="avatar-uploader"
                action="http://real-think.jmwl51.com//admin/Ajaxs/upload"
                :show-file-list="false"
                :on-success="handleAvatarSuccess"
                :before-upload="beforeAvatarUpload"
              >
                <img v-if="form.avatar" :src="form.avatar" class="avatar" />
                <el-icon v-else class="avatar-uploader-icon"><Plus /></el-icon>
              </el-upload>
            </el-form-item>
```

## Interface calling mode



`this.$ api. getRule(); `