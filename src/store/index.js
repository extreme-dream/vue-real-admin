/**
 * Vuex状态管理
 */
import { createStore } from 'vuex'
import mutations from './mutations'
// import tagsView from './tagsView'
import storage from '../utils/storage' 
//前面state，mutations，getters，actions...省略

const state = {
    userInfo : "" || storage.getItem("userInfo"), // 获取用户信息
    tagsview : [] || storage.getItem("tagsview"), // 获取用户信息 
}
export default createStore({
    state,
    mutations 
})