/**
 * Mutations业务层数据提交
 */
import storage from "../utils/storage";
import utils from "../utils/utils";

export default {
    saveUserInfo(state,userInfo) {
        state.userInfo = userInfo;
        storage.setItem('userInfo',userInfo)
    },
    changeMenuList(state, menuList) {
        //列表菜单
        state.menuList = menuList;
        storage.setItem("menuList", menuList);
    }, 
    pushtags(state,val){
        //如果等于-1说明tabs不存在那么插入，否则什么都不做
        let z = utils.getStorage("tagsview");
         if(typeof(z)!="undefined"&&z!=""&&z!=null){
          state.tagsview=  z;
         }else{
          state.tagsview=[
            {
              "apipath": "dashboard/index",
              "childlist": [],
              "component": 0,
              "createtime": 1491635035,
              "icon": "user-filled",
              "id": 1,
              "ismenu": 1,
              "menuCode": 0,
              "name": "welcome",
              "pid": 0,
              "remark": "",
              "spacer": "",
              "status": "normal",
              "title": "控制台",
              "updatetime": 1652348263,
              "weigh": 9999
            }
             
                                  
            ];
         }
        
          let result = state.tagsview.findIndex(item => item.name === val.name);
       
       
        console.log(result,"有")
        if(result === -1&&typeof(val)!="undefined"&&val!=""&&val!=null){
           
          console.log("没有",state.tagsview)
            state.tagsview.push(val);
            // storage.setItem('tagsview',state.tagsview)
            utils.setStorage("tagsview", state.tagsview)
        }else{
     
            console.log(result,"有")
        }
         
        console.log(state.tagsview,"最新的标签组");

       


        // state.tagsview = val;
        // storage.setItem('tagsview',val)
      },
       //关闭标签
    closeTab(state, val) {
        //同上，找角标，然后用角标的位置对应删除一位。splice：这是数组的删除方法
        let result = state.tagsview.findIndex(item => item.name === val.name)
        state.tagsview.splice(result, 1);
        utils.setStorage("tagsview", state.tagsview)
    },
    //关闭所有tagsview标签
      cleartagsview(state,val){
        //清空数组
        state.tagsview=[]
        // storage.setItem("tagsview", state.tagsview);
        
        utils.delStorage("tagsview")
        //跳转到首页,val接受传过来的当前路由
        // if(val !== "/index"){
        //   router.push({path:"/index"})
        // }
      },
      //改变tagsview显示隐藏
      changeisshow(state){
        state.isCollapse=!state.isCollapse
      }
}