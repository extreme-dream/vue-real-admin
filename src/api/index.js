/**
 * api管理
 */
import request from './../utils/request'
 
export default {
    // 登录
    login(params) {
        return request({
            url: '/admin/index/login',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //获取菜单
    getMenuList(params){
        return request({
            url: '/admin/index/index',
            method: 'get',
            data:{},
            mock:true
        })
    },
    //获取首页控制器信息
    getDashboard(){
        return request ({
            url:'/admin/dashboard/index',
            method:'post', 
            mock:true
        })
    },
    //获取配置内容
    getConfigList(){
        return request({
            url: 'admin/general.Configs/index',
            method: 'post',
            
            mock:true
        })
    },
    //修改配置
    setConfigList(params){
        return request({
            url: 'admin/general.Configs/edit',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //获取个人信息和个人操作日志
    getprofileList(params){
        return request({
            url: '/admin/general.profile/index',
            method: 'post',
            
            data: params,
            mock:true
        })
    },
    //设置个人信息
     setprofileList(params){
        return request({
            url: '/admin/general.profile/update',
            method: 'post',
            data: params,
            mock:true
        })
    },

    //获取数据库信息
    getDatabaseList(params){
        return request({
            url: '/admin/general.Database/index',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //优化表
    optimizeDatabase(params){
        return request({
            url: '/admin/general.Database/optimize',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //修复表
    repairDatabase(params){
        return request({
            url: '/admin/general.Database/repair',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //备份表
    dbbackup(params){
        return request({
            url: '/admin/general.Database/dbbackup',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //获取备份信息
    getbackuplstList(params){
        return request({
            url: '/admin/general.Database/backuplst',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //还原
    restore(params){
        return request({
            url: '/admin/general.Database/restore',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //删除备份文件
    delbackup(params){
        return request({
            url: '/admin/general.Database/delbackup',
            method: 'post',
            data: params,
            mock:true
        })
    },
    //管理员日志
    admin_log(params){
        return request({
            url: '/admin/auth.admin_log/index',
            method: 'post',
            data: params,
            mock:true
        })
    },
    

/**插件开始 */
///admin/Addon/index 查看本地插件列表
    getLocalAddonList(params){
        return request ({
            url:'admin/Addon/index',
            method:'post',
            data:params,
            mock:true
        })
    },
///离线安装
    getLocalinstallAddon(params){
        return request ({
            url:'admin/Addon/lxinstall',
            method:'post',
            data:params,
            mock:true
        })
    },

///卸载
    getLocaluninstallAddon(params){
        return request ({
            url:'admin/Addon/uninstall',
            method:'post',
            data:params,
            mock:true
        })
    },
    // 配置
    getconfig(params){
        return request ({
            url:'admin/Addon/config',
            method:'get',
            data:params,
            mock:true
        })
    },
    // 配置
    setconfig(params,name){
        return request ({
            url:'admin/Addon/config?name='+name,
            method:'post',
            data:params,
            mock:true
        })
    },
/**插件结束 */
/************/
    
    //获取权限节点
    getRule(params={status:"norma"}){
        return request ({
            url:'/admin/auth.rule/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增权限节点
    addRule(params={status:"norma"}){
        return request ({
            url:'/admin/auth.rule/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editRuleFind(params){
        return request ({
            url:'/admin/auth.rule/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改权限节点
    editRule(params,ids){
        return request ({
            url:'/admin/auth.rule/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除权限节点
    delRule(params){
        return request ({
            url:'/admin/auth.rule/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****菜单结束 */


/******角色管理开始****/
    
    //获取列表
    getGroup(params={status:"norma"}){
        return request ({
            url:'/admin/auth.group/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增
    addGroup(params={status:"norma"}){
        return request ({
            url:'/admin/auth.group/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editGroupFind(params){
        return request ({
            url:'/admin/auth.group/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改 
    editGroup(params,ids){
        return request ({
            url:'/admin/auth.group/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除 
    delGroup(params){
        return request ({
            url:'/admin/auth.group/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****角色结束 */

/******管理员管理开始****/
    
    //获取列表
    getAdmin(params={status:"norma"}){
        return request ({
            url:'/admin/auth.admin/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增
    addAdmin(params={status:"norma"}){
        return request ({
            url:'/admin/auth.admin/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editAdminFind(params){
        return request ({
            url:'/admin/auth.admin/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改 
    editAdmin(params,ids){
        return request ({
            url:'/admin/auth.admin/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除 
    delAdmin(params){
        return request ({
            url:'/admin/auth.admin/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****管理员结束 */
/****************************************************************************************** */


/************/
    
    //获取权限节点
    getUserRule(params={status:"norma"}){
        return request ({
            url:'/admin/user.rule/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增权限节点
    addUserRule(params={status:"norma"}){
        return request ({
            url:'/admin/user.rule/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editUserRuleFind(params){
        return request ({
            url:'/admin/user.rule/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改权限节点
    editUserRule(params,ids){
        return request ({
            url:'/admin/user.rule/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除权限节点
    delUserRule(params){
        return request ({
            url:'/admin/user.rule/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****菜单结束 */


/******角色管理开始****/
    
    //获取列表
    getUserGroup(params={status:"norma"}){
        return request ({
            url:'/admin/user.group/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增
    addUserGroup(params={status:"norma"}){
        return request ({
            url:'/admin/user.group/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editUserGroupFind(params){
        return request ({
            url:'/admin/user.group/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改 
    editUserGroup(params,ids){
        return request ({
            url:'/admin/user.group/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除 
    delUserGroup(params){
        return request ({
            url:'/admin/user.group/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****角色结束 */

/******管理员管理开始****/
    
    //获取列表
    getUser(params={status:"norma"}){
        return request ({
            url:'/admin/user.user/index',
            method:'post', 
            data: params,
            mock:true
        })
    },

    //新增
    addUser(params={status:"norma"}){
        return request ({
            url:'/admin/user.user/add',
            method:'post', 
            data: params,
            mock:true
        })
    },

 
    //查看单条数据
    editUserFind(params){
        return request ({
            url:'/admin/user.user/edit',
            method:'get', 
            data: params,
            mock:true
        })
    },
    //修改 
    editUser(params,ids){
        return request ({
            url:'/admin/user.user/edit?ids='+ids,
            method:'post', 
            data: params,
            mock:true
        })
    },
    //删除 
    delUser(params){
        return request ({
            url:'/admin/user.user/del',
            method:'get', 
            data: params,
            mock:true
        })
    },

/*****管理员结束 */
/************************************************************************** */


/************************************************************************** */


/** 广告位列表*/

Adszone(params) {
    return request({
        url: '/admin/general.Adszone/index',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 添加广告位*/

addAdszone(params) {
    return request({
        url: '/admin/general.Adszone/add',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 查看广告位*/

getAdszonefind(params) {
    return request ({
        url:'/admin/general.Adszone/edit',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 修改广告位*/

editAdszone(params,ids) {
    return request ({
        url:'/admin/general.Adszone/edit?ids='+ids,
        method:'post', 
        data: params,
        mock:true
    })
},

/** 删除广告位*/

delAdszone(params) {
    return request ({
        url:'/admin/general.Adszone/del',
        method:'get', 
        data: params,
        mock:true
    })
},



/** 广告列表*/

Ads(params,$ids) {
    return request({
        url: '/admin/general.Adszone/ads?ids='+$ids,
        method: 'post',
        data:params,
        mock:true
    })
},


/** 添加广告*/

adsadd(params) {
    return request({
        url: '/admin/general.Adszone/ads_add',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 查看广告*/

getAdsfind(params) {
    return request ({
        url:'/admin/general.Adszone/ads_edit',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 修改广告位*/

editAds(params,$ids) {
    return request ({
        url:'/admin/general.Adszone/ads_edit?ids='+$ids,
        method:'post', 
        data: params,
        mock:true
    })
},

/** 删除广告位*/

delAds(params) {
    return request ({
        url:'/admin/general.Adszone/ads_del',
        method:'get', 
        data: params,
        mock:true
    })
},


/************************************************************************** */



/************************************************************************** */


/** 友情链接*/

Link(params) {
    return request({
        url: '/admin/general.Link/index',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 添加 友情链接 位*/

addLinkzone(params) {
    return request({
        url: '/admin/general.Link/add',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 查看 友情链接位*/

getLinkzfind(params) {
    return request ({
        url:'/admin/general.Link/edit',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 修改 友情链接位*/

editzLink(params,ids) {
    return request ({
        url:'/admin/general.Link/edit?ids='+ids,
        method:'post', 
        data: params,
        mock:true
    })
},

/** 删除友情链接位*/

delLinkzone(params) {
    return request ({
        url:'/admin/general.Link/del',
        method:'get', 
        data: params,
        mock:true
    })
},



/** 广告列表*/

Linkindex(params,$ids) {
    return request({
        url: '/admin/general.Link/linkindex?ids='+$ids,
        method: 'post',
        data:params,
        mock:true
    })
},


/** 添加广告*/

Linkadd(params) {
    return request({
        url: '/admin/general.Link/link_add',
        method: 'post',
        data:params,
        mock:true
    })
},


/** 查看广告*/

getLinkfind(params) {
    return request ({
        url:'/admin/general.Link/link_edit',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 修改广告位*/

editLink(params,$ids) {
    return request ({
        url:'/admin/general.Link/link_edit?ids='+$ids,
        method:'post', 
        data: params,
        mock:true
    })
},

/** 删除广告位*/

delLink(params) {
    return request ({
        url:'/admin/general.Link/link_del',
        method:'get', 
        data: params,
        mock:true
    })
},


/************************************************************************** */
/****************栏目********************************************************** */
/** 列表*/

Cate(params) {
    return request ({
        url:'/admin/qy.Cate/index',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 添加*/

addCate(params) {
    return request ({
        url:'/admin/qy.Cate/add',
        method:'post', 
        data: params,
        mock:true
    })
},
/** 查看*/

getCateFind(params) {
    return request ({
        url:'/admin/qy.Cate/edit',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 修改*/

editCate(params,$ids) {
    return request ({
        url:'/admin/qy.Cate/edit?ids='+$ids,
        method:'post', 
        data: params,
        mock:true
    })
},
/** 删除*/

delCate(params) {
    return request ({
        url:'/admin/qy.Cate/del',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 回收站*/

RecycleCate(params) {
    return request ({
        url:'/admin/qy.Cate/Recycle',
        method:'get', 
        data: params,
        mock:true
    })
},

/** 恢复*/

restorerecycleCate(params) {
    return request ({
        url:'/admin/qy.Cate/restorerecycle',
        method:'get', 
        data: params,
        mock:true
    })
},

/** 删除*/

destroyCate(params) {
    return request ({
        url:'/admin/qy.Cate/destroy',
        method:'post', 
        data: params,
        mock:true
    })
}, 

/************************************************************************** */
/****************文章********************************************************** */
/** 列表*/

Article(params) {
    return request ({
        url:'/admin/qy.Article/index',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 添加*/

addArticle(params) {
    return request ({
        url:'/admin/qy.Article/add',
        method:'post', 
        data: params,
        mock:true
    })
},
/** 查看*/

getArticleFind(params) {
    return request ({
        url:'/admin/qy.Article/edit',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 修改*/

editArticle(params,$ids) {
    return request ({
        url:'/admin/qy.Article/edit?ids='+$ids,
        method:'post', 
        data: params,
        mock:true
    })
},
/** 删除*/

delArticle(params) {
    return request ({
        url:'/admin/qy.Article/del',
        method:'get', 
        data: params,
        mock:true
    })
},


/** 回收站*/

RecycleArticle(params) {
    return request ({
        url:'/admin/qy.Article/Recycle',
        method:'get', 
        data: params,
        mock:true
    })
},

/** 恢复*/

restorerecycleArticle(params) {
    return request ({
        url:'/admin/qy.Article/restorerecycle',
        method:'get', 
        data: params,
        mock:true
    })
},

/** 删除*/

destroyArticle(params) {
    return request ({
        url:'/admin/qy.Article/destroy',
        method:'post', 
        data: params,
        mock:true
    })
}, 

/************************************************************************** */
/****************单页********************************************************** */
/** 列表*/

Single(params) {
    return request ({
        url:'/admin/qy.Single/index',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 添加*/

addSingle(params) {
    return request ({
        url:'/admin/qy.Single/add',
        method:'post', 
        data: params,
        mock:true
    })
},
/** 查看*/

getSingleFind(params) {
    return request ({
        url:'/admin/qy.Single/edit',
        method:'get', 
        data: params,
        mock:true
    })
},
/** 修改*/

editSingle(params,$ids) {
    return request ({
        url:'/admin/qy.Single/edit?ids='+$ids,
        method:'post', 
        data: params,
        mock:true
    })
},
 
 

/************************************************************************** */
/************************************************************************** */
/** 自动生成*/

command(params) {
    return request({
        url: '/admin/Command/Command',
        method: 'post',
        data:params,
        mock:true
    })
},

/* */
    noticeCount(params) {
        return request({
            url: '/admin/Command/command',
            method: 'post',
            data:{},
            mock:true
        })
    },
    
    getMenuListData(params = { menuState: 1 }){
        return request({
            url: '/users/getPermissionList',
            method: 'get',
            data:params,
            mock:true
        })
    },
    getUserList(params) {
        return request({
            url: '/users/list',
            method: 'get',
            data: params,
            mock:true
        })
    },
    getUserAllList(params) {
        return request({
            url: '/users/all/list',
            method: 'get',
            data: params,
            mock:true
        })
    },
    userDel(params){
        return request({
            url: '/users/delete',
            method: 'post',
            data: params,
            mock:true
        })
    },
    getRoleAllList(){
        return request({
            url:'/roles/allList',
            method:'get',
            data:{},
            mock:true
        })
    },
    getRoleList(){
        return request({
            url:'/roles/List',
            method:'get',
            data:{},
            mock:true
        })
    },
    getDeptList(){
        return request({
            url:'/dept/list',
            method:'get',
            data:{},
            mock:true
        })
    },
    userSubmit(params){
        return request({
            url:'/users/operate',
            method:'post',
            data:params,
            mock:true
        })
    },
    menuSubmit(params) {
        return request({
            url: '/menu/operate',
            method: 'post',
            data: params,
            mock: true
        })
    },
    updatePermission(params) {
        return request({
            url: '/roles/update/permission',
            method: 'post',
            data: params,
            mock: true
        })
    }
}