 
import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../components/Home.vue'

const routes = [
    {
        name: 'home',
        path: '/',
        meta: { title:'首页' },
        component: Home,
        redirect:'/welcome',
        children: [
            {
                name: 'welcome',
                path: '/welcome',
                meta: { title:'欢迎体验Vue3项目' },
                component:() => import('./../views/Welcome.vue')
            },
            {
                name: 'profile',
                path: '/profile',
                meta: { title:'个人信息' },
                component:() => import('./../views/general/profile/index.vue')
            },
            {
                name: 'database',
                path: '/database',
                meta: { title:'数据库管理' },
                component:() => import('./../views/general/database/index.vue'),
                children: [
                    {  
                        name: 'getbackuplstList',
                        path: '/getbackuplstList',
                        meta: {
                            title: '管理员管理'
                        },
                        component: () => import('./../views/general/database/getbackuplstList.vue')
                    },
                     
                ]
            },
            {
                name: 'config',
                path: '/config',
                meta: { title:'系统配置' },
                component:() => import('./../views/general/configset/index.vue')
            },
            
           
            {
                name: 'admin',
                path: '/admin',
                meta: {
                    title: '管理员管理'
                },
                component: () => import('./../views/auth/admin/index.vue'),
                children: [
                    {  
                        name: 'admin/add',
                        path: '/admin/add',
                        meta: {
                            title: '管理员管理'
                        },
                        component: () => import('./../views/auth/admin/add.vue')
                    },
                    {  
                        name: 'admin/edit',
                        path: '/admin/edit',
                        meta: {
                            title: '管理员管理'
                        },
                        component: () => import('./../views/auth/admin/edit.vue')
                    },
                ]
            },
            {
                name: 'group',
                path: '/group',
                meta: {
                    title: '角色管理'
                },
                component: () => import('./../views/auth/group/index.vue'),
                children: [
                    {  
                        name: 'group/add',
                        path: '/group/add',
                        meta: {
                            title: '角色管理'
                        },
                        component: () => import('./../views/auth/group/add.vue')
                    },
                    {  
                        name: 'group/edit',
                        path: '/group/edit',
                        meta: {
                            title: '角色管理'
                        },
                        component: () => import('./../views/auth/group/edit.vue')
                    },
                ]
            },
            {
                name: 'rule',
                path: '/rule',
                meta: {
                    title: '菜单管理'
                },
                component: () => import('./../views/auth/rule/index.vue'),
                children: [
                    {  
                        name: 'ruleadd',
                        path: '/rule/add',
                        meta: {
                            title: '菜单管理'
                        },
                        component: () => import('./../views/auth/rule/add.vue')
                    },
                    {  
                        name: 'ruleedit',
                        path: '/rule/edit',
                        meta: {
                            title: '菜单管理'
                        },
                        component: () => import('./../views/auth/rule/edit.vue')
                    },
                ]
            },
/******************************************************************************************** */

{
    name: 'user',
    path: '/user',
    meta: {
        title: '管理员管理'
    },
    component: () => import('./../views/user/user/index.vue'),
  
},
{
    name: 'usergroup',
    path: '/usergroup',
    meta: {
        title: '角色管理'
    },
    component: () => import('./../views/user/group/index.vue'),
 
},
{
    name: 'userrule',
    path: '/userrule',
    meta: {
        title: '菜单管理'
    },
    component: () => import('./../views/user/rule/index.vue'),
 
},
/****************************************************************************************************** */
/****************************************************************************************************** */

//广告位列表
{  
    name: 'adszone',
    path: 'adszone',
    meta: {
        title: 'adszone'
    },
    component: () => import('./../views/general/adszone/index.vue')
},
 
 //友情链接位列表
{  
    name: 'link',
    path: 'link',
    meta: {
        title: 'link'
    },
    component: () => import('./../views/general/link/index.vue')
},
 
 
 //栏目列表
{  
    name: 'cate',
    path: 'cate',
    meta: {
        title: 'link'
    },
    component: () => import('./../views/qy/cate/index.vue')
},
 
 //图文列表
{  
    name: 'image',
    path: 'image',
    meta: {
        title: 'link'
    },
    component: () => import('./../views/qy/image/index.vue')
},
//单页列表
{  
   name: 'fragment',
   path: 'fragment',
   meta: {
       title: 'link'
   },
   component: () => import('./../views/qy/fragment/index.vue')
},
 
 //文章列表
{  
    name: 'article',
    path: 'article',
    meta: {
        title: 'link'
    },
    component: () => import('./../views/qy/article/index.vue')
},

/****************************************************************************************************** */
//自动生成
{  
    name: 'command',
    path: 'command',
    meta: {
        title: 'dome'
    },
    component: () => import('./../views/command/crm.vue')
},
{
                name: 'dome',
                path: '/dome',
                meta: {
                    title: 'dome'
                },
                component: () => import('./../views/dome/index.vue'),
                children: [
                  
                    {  
                        name: 'domeedit',
                        path: '/dome/edit',
                        meta: {
                            title: 'dome'
                        },
                        component: () => import('./../views/dome/edit.vue')
                    },
                ]
            },   {
                name: 'apidoc',
                path: '/apidoc',
                meta: {
                    title: 'apidoc'
                },
                component: () => import('./../views/dome/apidoc.vue'),
                
            },
            
            {  
                name: 'domeadd',
                path: '/dome/add',
                meta: {
                    title: 'dome'
                },
                component: () => import('./../views/dome/add.vue')
            },
             
      
             
            
            {
                name: 'adminlog',
                path: '/adminlog',
                meta: {
                    title: '日志管理'
                },
                component: () => import('./../views/auth/adminlog/index.vue')
            },
            {
                name: 'addon',
                path: '/addon',
                meta: {
                    title: '插件管理'
                },
                component: () => import('./../views/addon/index.vue')
            },
        ]
    },
    {
        name: 'login',
        path: '/login',
        meta: { title:'登陆' },
        component:() => import('./../views/Login.vue')
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})
export default router