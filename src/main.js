import { createApp } from 'vue'

import "./assets/theme.scss"
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
 
import * as ElIcons from '@element-plus/icons'

import App from './App.vue'
import router from './router'
import request from './utils/request';
import storage from './utils/storage';
import api from './api';
import store from './store' 
import * as echarts from 'echarts'
import 'default-passive-events' // Google公布了一个让页面滑动更流畅的新特性Passive Event Listeners。


// 导入富文本编译器
import VueQuillEditor from 'vue-quill-editor'
 
 
// require styles 导入富文本编译器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
 
  
const app = createApp(App);
app.config.globalProperties.$request = request;
app.config.globalProperties.$storage = storage;
app.config.globalProperties.$api = api;

// app.config.globalProperties.$echarts = echarts
// app.$echarts = echarts

app.config.globalProperties.$echarts= echarts;
 
for (const name in ElIcons){
	app.component(name,(ElIcons)[name])
}
import { quillEditor } from 'vue-quill-editor'

export default {
  components: {
    quillEditor
  }
}
 
app.use(router).use(store).use(VueQuillEditor).use(ElementPlus,{size:'small'}).mount('#app');
 
 
 