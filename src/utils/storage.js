/**
 * Storage二次封装
 * @author Czl
 */
import config from '../config'
export default {
  setItem(key,val){
    let storage = this.getStroage();
    storage[key] = val;
    window.localStorage.setItem(config.namespace,JSON.stringify(storage))  
  },
  getItem(key){
    return this.getStroage()[key]
  },
  getStroage(){
    return JSON.parse(window.localStorage.getItem(config.namespace) || "{}");
  },
  clearItem(item,key){
    let storage = this.getStroage()
    if(key) {
      delete storage[item][key]
    }else {
      delete storage[item]
    }
    window.localStorage.setItem(config.namespace,JSON.stringify(storage))
  },
  clearAll(){
    window.localStorage.clear()
  }
}