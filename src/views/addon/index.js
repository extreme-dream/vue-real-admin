import utils from "../../utils/utils";

import ActionButton from "../../components/ActionButton.vue";
import QueryForm from "../../components/QueryForm.vue";
import JmTb from "../../components/JmTb.vue";

import config from "./config.vue"; 
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTb,config},
    data() {
        return {
       
          compontedshowModal:"config",//弹窗组件
            showModal: false,//表单是否显示
            edit_id:10,
            extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                },
            dataList:  [],
            page:{
                total:500,
                pageSize:10 
            },  
            columns: [  
                    
                  

                    {label: "标识",  prop: "name",},
                    {label: "名称",      prop: "title",  }, 
                    {label: "介绍",      prop: "intro",   }, 
                    {label: "作者",  prop: "author", },
                    
                    {label: "创建时间",  prop: "create_time",  
                        formatter(row, column, value) {
                          return utils.formateDate(new Date(value));
                        },},
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      const res =   await this.$api.getLocalAddonList();  
      this.dataList =res.addons; 
  
    },
      
    async uninstallthinkAddon(name){
           await this.$api.getLocaluninstallAddon({ name: name});
             
           this.getDataList()
           
           this.$message.success("删除成功2");
        },
        
    // 新增菜单
    add(type, row) {
      this.showModal = true;
      this.action = "add";
      
				this.$router.push('/addon/add')
      // if (type == 2) {
      //   this.menuForm.pid = [...row.pid, row._id].filter(
      //     (item) => item
      //   );
      // }
    },
     
      //修改
    edit(name) {
        this.showModal = true;
        this.action = "配置";
        this.edit_id=name
    
    },
    // 菜单操作-提交
    handleSubmit() {
      this.$refs.dialogForm.validate(async (valid) => {
        if (valid) {
          let { action, menuForm } = this;
          let params = { ...menuForm, action };
          let res = await this.$api.menuSubmit(params);
          this.showModal = false;
          this.$message.success("操作成功");
          this.handleReset("dialogForm");
          this.getMenuList();
        }
      });
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false;
      
			this.$router.push('/addon')
      this.handleReset("dialogForm");
    },
  },
};