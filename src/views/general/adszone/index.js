import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import JmTb from "../../../components/JmTb.vue";
import add from "./add.vue";
import edit from "./edit.vue";
import adsindex from "./adsindex.vue";
export default {
  name: "index",
  components: { ActionButton, QueryForm, JmTable,JmTb, add, edit,adsindex },
  data() {
    return {
      compontedshowModal: "add",//弹窗组件
      showModal: false,//表单是否显示
      edit_id: 10,
      dataList: [],
      ids: {},
      where: {
        email: "",
        mobile: "",
        username: "",
        status: "",

      },
      page: {
        total: 500,
        pageSize: 10,
        page: 1
      },
      columns: [
        { label: "#", prop: "id", width: 150, type: "selection" },
        { label: "标题", prop: "name", },
        { label: "唯一标识", prop: "mark", },
        { label: "类型", prop: "type", },
        { label: "高", prop: "height", },
        { label: "宽", prop: "width", },
        {
          label: "创建时间", prop: "createtime",
          formatter(row, column, value) {
            return utils.formateDate(new Date(value));
          },
        },
      ],//行参数
    }
  },
  mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() {
      const res = await this.$api.Adszone({ page: this.page.page, limit: this.page.pageSize });
      this.dataList = res.rows;
      this.page.total = res.total
    },
    //分页的时候获取数据
    async handleCurrentChange(val) {

      let list = await this.$api.Adszone({ page: val, limit: this.page.pageSize });
      this.dataList = list.rows;
      this.page.total = list.total;
    },
    //删除
    async del(_id){
      await this.$api.delAdszone({ ids: _id });
      this.getDataList();
      this.$message.success("删除成功2");
    },
    //删除
    async alldel(_id) {
      var ids = "";
      for (let item of this.ids) {
        ids += item + ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
      await this.$api.delAdszone({ ids: ids });
      this.getDataList();
      this.$message.success("删除成功2");

    },
    setids(ids) {
      this.ids = ids
    },
    //查询
    async handleSubmit(val) {

      let list = await this.$api.Adszone({ page: val, limit: this.page.pageSize, where: this.where });
      this.dataList = list.rows;
      this.page.total = list.total;
    },

    // 广告管理
    ads(row) {
      this.edit_id = row
      this.showModal = true;
      console.log(row)
      this.action = "adsindex";
      this.compontedshowModal = "adsindex";
    },

    // 新增菜单
    add(  row) {
      this.showModal = true;
      this.action = "add";
      this.compontedshowModal = "add";
    },
    //修改
    edit(row) {
      this.showModal = true;
      this.action = "edit";
      this.edit_id = row
      this.compontedshowModal = "edit";
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false;
      this.getDataList(); 
    },
    closeDialog() {
      this.showModal = false;
      this.getDataList(); 
    }
  },
};