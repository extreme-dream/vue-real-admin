import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue"; 

import { getMockDataList } from './datelist';

import JmTb from "../../../components/JmTb.vue";
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTb},
 
    data() {
        return {
            form :{
                name: '1',
                nickname:"1",
                password:"password",
                avatar: '', 
                },

    
            showModal: false,//表单是否显示
            tablename:10,
            extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                },
            dataList:  [],
            columns: [  
            
                
                    {label: "表备注",  prop: "comment",}, 
                    {label: "引擎",  prop: "engine", },
                    {label: "表名",  prop: "name",   },
                    {label: "行格式",  prop: "row_format", },   
                    {label: "数据",  prop: "rows",  }, 
                    {label: "创建时间",  prop: "create_time",  
                        formatter(row, column, value) {
                          return utils.formateDate(new Date(value));
                        },},
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() {  
    
        const res = await this.$api.getDatabaseList(); 

        let dataList= res;
         
        this.dataList =dataList;
    },
    //表优化
    optimize(tablename){
      const res =   this.$api.optimizeDatabase({"tablename":tablename}); 
      // this.$message.success("删除成功2");
            
    },
    repair(tablename){
      const res =   this.$api.repairDatabase({"tablename":tablename}); 
      // this.$message.success("删除成功2");
            
    },
    dbbackup(tablename){
      const res =   this.$api.dbbackup({"tablename":tablename}); 
      this.$message.success("成功");
            
    },
    getbackuplstList(tablename){
      // const res =   this.$api.getbackuplstList({"tablename":tablename}); 
     this.tablename=tablename;
      this.showModal = true;
      this.action = "getbackuplstList"; 
      // 生成随机数
const a = Math.floor(Math.random() * (max - min + 1)) + min;
		 this.$router.push('/getbackuplstList?a='+a)
    },
      
    //删除
    async alldel(_id) {
        this.$message.success("删除成功all");
    //   await this.$api.menuSubmit({ _id, action: "delete" });
    //   this.$message.success("删除成功");
    //   this.getMenuList();
    },
    // 菜单操作-提交
    handleSubmit() {
      this.$refs.dialogForm.validate(async (valid) => {
        if (valid) {
          let { action, menuForm } = this;
          let params = { ...menuForm, action };
          let res = await this.$api.menuSubmit(params);
          this.showModal = false;
          this.$message.success("操作成功");
          this.handleReset("dialogForm");
          this.getMenuList();
        }
      });
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false;
      this.$router.push('/')
      this.handleReset("dialogForm");
    },
  },
};