import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue"; 

import { getMockDataList } from './datelist';

import JmTb from "../../../components/JmTb.vue";
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTb},
 
    data() {
        return {  
            dataList:  [],
            columns: [  
            
                
                    {label: "文件",  prop: "filename",},    
                    {label: "大小",  prop: "size",  }, 
                    {label: "创建时间",  prop: "time",  
                        formatter(row, column, value) {
                          return utils.formateDate(new Date(value));
                        },},
                ],//行参数
        }
    }, 
    props: ['tablename'],
    inject:['handleClose'],
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() {  
       console.log( this.tablename);
        const res = await this.$api.getbackuplstList({"tablename":this.tablename}); 
        // {part: 1, size: 4171, compress: "-", time: 1652250934, filename: "20220511-143534-1.sql"}
        let dataList= res.list;
         
        this.dataList =dataList;
    },
    //表还原
    restore(time){
      const res =   this.$api.restore({"tablename":this.tablename,"time":time}); 
      this.$message.success("成功2");
            
    },
    delbackup(time){
        const res =   this.$api.delbackup({"tablename":this.tablename,"time":time}); 
        const  resz =   this.$api.getbackuplstList({"tablename":this.tablename});  
        this.dataList =resz.list;
        this.$message.success("成功2");
              
      },
      
  },
};