import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import add from "./adsadd.vue";
import edit from "./adsedit.vue";
export default {
  name: "index",
  components: { ActionButton, QueryForm, JmTable, add, edit },
  data() {
    return {
      compontedshowModal: "add",//弹窗组件
      showModal: false,//表单是否显示
      adsedit_id: 10,
      dataList: [],
      ids: {},
      where: {
        email: "",
        mobile: "",
        username: "",
        status: "",

      },
      page: {
        total: 500,
        pageSize: 10,
        page: 1
      },
      columns: [ 
        { label: "#", prop: "id", width: 150, type: "selection" },
        { label: "标题", prop: "title", },
        { label: "连接", prop: "mark",typr:"link" },
        { label: "排序", prop: "weigh", },
        { label: "广告位id", prop: "zone_id", }, 
        { label: "跳转类型", prop: "target", }, 
        {label: "图片",        prop: "imageurl",    type:"avatar"   },
        {
          label: "启动时间", prop: "effectime",
          formatter(row, column, value) {
            return utils.formateDate(new Date(value));
          },
        },
        {
          label: "到期时间", prop: "expiretime",
          formatter(row, column, value) {
            return utils.formateDate(new Date(value));
          },
        },
        {
          label: "创建时间", prop: "createtime",
          formatter(row, column, value) {
            return utils.formateDate(new Date(value));
          },
        },
      ],//行参数
    }
  },
  //在export default中添加即可不用管顺序
  watch: {
    //监听内容
    edit_id() { 
     this.getDataList();
   }
 },  
 props: ['edit_id'],
 inject:['handleClose'], 
 
  mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() {
      const res = await this.$api.Linkindex({ page: this.page.page, limit: this.page.pageSize },this.edit_id);
     
 
        this.dataList = res.rows;
        
      this.page.total = res.total;
    },
    //分页的时候获取数据
    async handleCurrentChange(val) {

      let list = await this.$api.Linkindex({ page: val, limit: this.page.pageSize });
      this.dataList = list.rows;
      this.page.total = list.total;
    },
    //删除
    async del(_id){
      await this.$api.delLink({ ids: _id });
      this.getDataList();
      this.$message.success("删除成功2");
    },
    //删除
    async alldel(_id) {
      var ids = "";
      for (let item of this.ids) {
        ids += item + ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
      await this.$api.delLink({ ids: ids });
      this.getDataList();
      this.$message.success("删除成功2");

    },
    setids(ids) {
      this.ids = ids
    },
    //查询
    async handleSubmit(val) {

      let list = await this.$api.Linkindex({ page: val, limit: this.page.pageSize, where: this.where });
      this.dataList = list.rows;
      this.page.total = list.total;
    },


    // 新增菜单
    add(type, row) {
      this.showModal = true;
      this.action = "add";
      this.compontedshowModal = "add";
    },
    //修改
    edit(row) {
      this.showModal = true;
      this.action = "edit";
      this.adsedit_id = row
      this.compontedshowModal = "edit";
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false;
      this.getDataList(); 
    },
    closeDialog() {
      this.showModal = false;
      this.getDataList(); 
    }
  },
};