import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";

import JmInput from "../../../components/form/JmInput.vue";
import JmEnum from "../../../components/form/JmEnum.vue";
 

import JmTb from "../../../components/JmTb.vue";
export default {
    name: "menu",
    components: { ActionButton, QueryForm, JmTb,JmInput,JmEnum },

    data() {
        return {
            form: {
                username: '1',
                nickname: "1",
                password: "",
                avatar: '',
            },


            showModal: false,//表单是否显示
            edit_id: 10,
            extend: {
                index_url: 'auth/admin/index',
                add_url: 'auth/admin/add',
                edit_url: 'auth/admin/edit',
                del_url: 'auth/admin/del',
                multi_url: 'auth/admin/multi',
            },
            dataList: [],
            page: {
                total: 500,
                pageSize:10,
                page:0,
            },
            columns: [
 
                { label: "用户名", prop: "username", },
                { label: "访问接口", prop: "url",   },
                { label: "标题", prop: "title", },
                { label: "ip", prop: "ip",   },
                {
                    label: "创建时间", prop: "createtime",
                    formatter(row, column, value) {
                        return utils.formateDate(new Date(value));
                    },
                },
            ],//行参数
        }
    },
    mounted() {
        this.getDataList();
    },
    methods: {
        reload(){
            location.reload()
        },
           
        handleAvatarSuccess(res){
            console.log(res)
            this.form.avatar=  res.data.allfile
        },
        //  初始化
        async getDataList() {
            const res = await this.$api.getprofileList(this.page); 
   
            this.form.username=res.user.username
            this.form.nickname=res.user.nickname
            // this.form.password=res.user.password
            this.form.avatar=res.user.avatar
            this.page.total= res.total;
            // let dataList = getMockDataList();
            // this.dataList = dataList.data;
            this.dataList = res.rows;
            
        },
        //分页的时候获取数据
        async handleCurrentChange(val) {

            let list = await this.$api.getprofileList({ page: val, pageSize: this.page.pageSize });
            this.dataList = list.rows;
            this.page.total = list.total;
        },
          // 菜单操作-提交
          onSubmit() {
            // this.$message.success("操作成功");
            let list = this.$api.setprofileList(this.form);
            
            // this.$refs.dialogForm.validate(async (valid) => {
            //     if (valid) {
            //         let { action, menuForm } = this;
            //         let params = { ...menuForm, action };
            //         let res = await this.$api.menuSubmit(params);
            //         this.showModal = false;
            //         this.$message.success("操作成功");
            //         this.handleReset("dialogForm");
            //         this.getMenuList();
            //     }
            // });
        },
    },
};