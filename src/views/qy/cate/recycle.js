import utils from "../../../utils/utils"; 
import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import JmTb from "../../../components/JmTb.vue";
export default {
  name: "index",
  components:{ActionButton,QueryForm,JmTable,JmTb },
    data() {
        return {
        
            showModal: false,//表单是否显示
            edit_id:10, 
            dataList:  [],
            ids:{},
            where:{
              email:"",
              mobile:"",
              username:"",
              status:"",
               
            },
            page:{
                total:500,
                pageSize:10,
                page:1
            },  
            columns: [ 
                      {label: "#",        prop: "id",  width: 50, type:"selection"   },
                      {label: "栏目名称",      prop: "cate_name",   }, 
                      {label: "唯一标识",  prop: "guid",},
                      {label: "类型",  prop: "cate_model",},
                      {label: "栏目链接",      prop: "cate_link",   }, 
                      {label: "栏目缩略图",      prop: "cate_img",  type:"avatar" ,}, 
                      {label: "导航",  prop: "is_nav", },
                      {label: "显示",  prop: "is_show", },
                      {label: "显示",  prop: "is_show", },
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  /*
  恢复 
    restorerecycleCate
  删除
    destroyCate

  */
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      const res =   await this.$api.RecycleCate({page:this.page.page,limit:this.page.pageSize});  
      this.dataList =res;
    },
       //分页的时候获取数据
    async  handleCurrentChange(val) {
   
        let list = await this.$api.RecycleCate({page:val,limit:this.page.pageSize});
        this.dataList = list.rows;
        this.page.total=list.total; 
      },
      //搜索
    async  handleSubmit(val) {
   
      let list = await this.$api.RecycleCate({page:val,limit:this.page.pageSize,where:this.where});
      this.dataList = list.rows;
      this.page.total=list.total; 
    },
    //删除
      async alldel(_id) {
            
        console.log(this.ids);
        //   this.$message.success("删除成功all");
        
        var ids = "";
        for (let item of this.ids) {
          ids += item+ ",";
        }
        //去掉最后一个逗号(如果不需要去掉，就不用写)
        if (ids.length > 0) {
          ids = ids.substr(0, ids.length - 1);
        }

        await this.$api.destroyCate({ids:ids}); 
        
        //   // console.log('id',_id);
        this.getDataList();
        
        this.$message.success("删除成功2");

      },
      setids(ids){
        this.ids=ids
      },
      async del(_id){ 
        await this.$api.destroyCate({ids:_id});  
         this.getDataList(); 
         this.$message.success("删除成功2");
      }, 
    
      async restorerecycleCate(_id){ 
        await this.$api.restorerecycleCate({ids:_id});  
         this.getDataList(); 
         this.$message.success("删除成功2");
      }, 
     
      
 
    // 弹框关闭
    handleClose() {
      this.showModal = false; 
      this.handleReset("dialogForm");
    },
     closeDialog() {
      this.showModal = false;
      this.getDataList(); 
      this.handleReset("dialogForm");
    }
  },
};