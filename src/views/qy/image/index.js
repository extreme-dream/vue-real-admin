import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import add from "./add.vue";
import edit from "./edit.vue";
import recycle from "./recycle.vue";
export default {
  name: "index",
  components:{ActionButton,QueryForm,JmTable,add,edit,recycle},
    data() {
        return {
          compontedshowModal:"add",//弹窗组件
            showModal: false,//表单是否显示
            edit_id:10, 
            dataList:  [],
            ids:{},
            where:{
              email:"",
              mobile:"",
              username:"",
              status:"",
               
            },
            page:{
                total:500,
                pageSize:10,
                page:1
            },  
            columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   },
                    {label: "标题",  prop: "title",},
                    {label: "浏览量",  prop: "views",},
                    {label: "缩略图",      prop: "art_img_big",  type:"avatar" ,}, 
                    {label: "栏目id",      prop: "cate_id",   }, 
                    {label: "昵称",      prop: "nickname",   }, 
                    {label: "组名",  prop: "groupname", }, 
                    {
                      label: "推荐",
                      prop: "is_recommend",
                      width: 90,
                      formatter(row, column, value) {
                        return {
                          "1": "正常",
                          "-1": "停用",
                        }[value];
                      },
                    },
                    {
                      label: "新",
                      prop: "is_news",
                      width: 90,
                      formatter(row, column, value) {
                        return {
                          "1": "正常",
                          "-1": "停用",
                        }[value];
                      },
                    },
                     
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      const res =   await this.$api.Article({page:this.page.page,limit:this.page.pageSize,cate_model:3});  
      this.dataList =res.data;
      this.page.total=res.total;
      this.page.pageSize=list.per_page; 
    },
       //分页的时候获取数据
    async  handleCurrentChange(val) {
   
        let list = await this.$api.Article({page:val,limit:this.page.pageSize,cate_model:3});
        this.dataList = list.data;
        this.page.total=list.total; 
        this.page.pageSize=list.per_page; 
      },
      async  handleSubmit(val) { 
        let list = await this.$api.Article({page:val,limit:this.page.pageSize,where:this.where,cate_model:3});
        this.dataList = list.data;
        this.page.total=list.total; 
        this.page.pageSize=list.per_page; 
      },
      Recycle() {
        this.showModal = true;
        this.action = "recycle";
        this.compontedshowModal="recycle";
    },
      async del(_id){ 
        /*查看单条数据*/
        await this.$api.delArticle({ids:_id});  
         this.getDataList();
         
         this.$message.success("删除成功2");
      }, 
     // 新增菜单
     add(type, row) {
          this.showModal = true;
          this.action = "add";
          this.compontedshowModal="add";
      },
      //修改
      edit(row) {
          this.showModal = true;
          this.action = "edit";
          this.edit_id=row
        this.compontedshowModal="edit";
      },
    //删除
    async alldel(_id) {
       
      console.log(this.ids);
      //   this.$message.success("删除成功all");
      
      var ids = "";
      for (let item of this.ids) {
        ids += item+ ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
   
      await this.$api.delArticle({ids:ids}); 
       
      //   // console.log('id',_id);
       this.getDataList();
       
       this.$message.success("删除成功2");
   
    },
    setids(ids){
      this.ids=ids
  },
 
    // 弹框关闭
    handleClose() {
      this.showModal = false; 
      this.getDataList(); 
      this.handleReset("dialogForm");
    },
     closeDialog() {
      this.showModal = false;
      this.getDataList(); 
      this.handleReset("dialogForm");
    }
  },
};