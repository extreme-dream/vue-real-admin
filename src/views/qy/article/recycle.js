import utils from "../../../utils/utils"; 
import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import JmTb from "../../../components/JmTb.vue";
export default {
  name: "index",
  components:{ActionButton,QueryForm,JmTable,JmTb },
    data() {
        return {
        
            showModal: false,//表单是否显示
            edit_id:10, 
            dataList:  [],
            ids:{},
            where:{
              email:"",
              mobile:"",
              username:"",
              status:"",
               
            },
            page:{
                total:500,
                pageSize:10,
                page:1
            },  
            columns: [ 
                    
              {label: "#",        prop: "id",  width: 150, type:"selection"   },
              {label: "标题",  prop: "title",},
              {label: "浏览量",  prop: "views",},
              {label: "缩略图",      prop: "art_img_big",  type:"avatar" ,}, 
              {label: "栏目id",      prop: "cate_id",   }, 
              {label: "昵称",      prop: "nickname",   }, 
              {label: "组名",  prop: "groupname", }, 
              {
                label: "推荐",
                prop: "is_recommend",
                width: 90,
                formatter(row, column, value) {
                  return {
                    "1": "正常",
                    "-1": "停用",
                  }[value];
                },
              },
              {
                label: "新",
                prop: "is_news",
                width: 90,
                formatter(row, column, value) {
                  return {
                    "1": "正常",
                    "-1": "停用",
                  }[value];
                },
              },
               
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  /*
  恢复 
    restorerecycleCate
  删除
    destroyCate

  */
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      const res =   await this.$api.RecycleArticle({page:this.page.page,limit:this.page.pageSize});  
      this.dataList =res;
    },
       //分页的时候获取数据
    async  handleCurrentChange(val) {
   
        let list = await this.$api.RecycleArticle({page:val,limit:this.page.pageSize});
        this.dataList = list.rows;
        this.page.total=list.total; 
      },
      //搜索
    async  handleSubmit(val) {
   
      let list = await this.$api.RecycleArticle({page:val,limit:this.page.pageSize,where:this.where});
      this.dataList = list.rows;
      this.page.total=list.total; 
    },
    //删除
      async alldel(_id) {
            
        console.log(this.ids);
        //   this.$message.success("删除成功all");
        
        var ids = "";
        for (let item of this.ids) {
          ids += item+ ",";
        }
        //去掉最后一个逗号(如果不需要去掉，就不用写)
        if (ids.length > 0) {
          ids = ids.substr(0, ids.length - 1);
        }

        await this.$api.destroyArticle({ids:ids}); 
        
        //   // console.log('id',_id);
        this.getDataList();
        
        this.$message.success("删除成功2");

      },
      setids(ids){
        this.ids=ids
      },
      async del(_id){ 
        await this.$api.destroyArticle({ids:_id});  
         this.getDataList(); 
         this.$message.success("删除成功2");
      }, 
    
      async restorerecycleCate(_id){ 
        await this.$api.restorerecycleArticle({ids:_id});  
         this.getDataList(); 
         this.$message.success("删除成功2");
      }, 
     
      
 
    // 弹框关闭
    handleClose() {
      this.showModal = false; 
      this.handleReset("dialogForm");
    },
     closeDialog() {
      this.showModal = false;
      this.getDataList(); 
      this.handleReset("dialogForm");
    }
  },
};