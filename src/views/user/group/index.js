import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import add from "./add.vue";
import edit from "./edit.vue";
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTable,add,edit},
    data() {
        return {
           compontedshowModal:"add",//弹窗组件
            showModal: false,//表单是否显示
            edit_id:10,
            where:{
              name:"", 
              status:"",
               
            }, 
            dataList:  [],
            ids:{},
            page:{
                total:500,
                pageSize:10,
                page:1,
            },  
            columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   },
   

                    {label: "角色名称",  prop: "name",},
                  
                    {
                      label: "菜单状态",
                      prop: "status",
                      width: 90,
                      formatter(row, column, value) {
                        return {
                          "1": "正常",
                          "0": "停用",
                        }[value];
                      },
                    },
                    {label: "创建时间",  prop: "createtime", },
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      
      let list = await this.$api.getUserGroup({page:this.page.page,limit:this.page.pageSize});
      this.dataList = list.rows;
      this.page.total=list.total; 
    },
 
    async del(_id){
           await this.$api.delUserGroup({ids: _id  });
            this.$message.success("删除成功2");
            
            console.log('id',_id);
           this.getDataList()
        },
    // 新增菜单
    // add(type, row) {
    //   this.showModal = true;
    //   this.action = "add";
      
		// 		this.$router.push('/group/add')
    //   // if (type == 2) {
    //   //   this.menuForm.pid = [...row.pid, row._id].filter(
    //   //     (item) => item
    //   //   );
    //   // }
    // },
    // //修改
    // edit(row) {
    //      this.showModal = true;
    //   this.action = "edit";
    //   this.edit_id=row
		// 		this.$router.push('/group/edit')
    //   // this.showModal = true;
    //   // this.action = "edit";
    //   // this.$nextTick(() => {
    //   //   Object.assign(this.menuForm, row);
    //   // });
    // },
     // 新增菜单
     add(type, row) {
      this.showModal = true;
      this.action = "add";
      this.compontedshowModal="add";
  },
  //修改
  edit(row) {
      this.showModal = true;
      this.action = "edit";
      this.edit_id=row
    this.compontedshowModal="edit";
  },
      // 菜单操作-提交
      async  handleSubmit(val) {
   
        let list = await this.$api.getUserGroup({page:val,limit:this.page.pageSize,where:this.where});
        this.dataList = list.rows;
        this.page.total=list.total; 
      },
    //删除
    async alldel(_id) {
      console.log(this.ids);
      //   this.$message.success("删除成功all");
      
      var ids = "";
      for (let item of this.ids) {
        ids += item+ ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
   
      await this.$api.delUserGroup({ids:ids}); 
       
      //   // console.log('id',_id);
       this.getDataList();
       
       this.$message.success("删除成功2");
    },
    // 菜单操作-提交
    // handleSubmit() {
    //   this.$refs.dialogForm.validate(async (valid) => {
    //     if (valid) {
    //       let { action, menuForm } = this;
    //       let params = { ...menuForm, action };
    //       let res = await this.$api.menuSubmit(params);
    //       this.showModal = false;
    //       this.$message.success("操作成功");
    //       this.handleReset("dialogForm");
    //       this.getMenuList();
    //     }
    //   });
    // },
    
    setids(ids){
      this.ids=ids
  },
    // 弹框关闭
    handleClose() {
 
      this.showModal = false;
      this.getDataList();
       
      // this.handleReset("dialogForm");
    },
    closeDialog() {
      this.showModal = false;
      this.getDataList(); 
      // this.handleReset("dialogForm");
    }
  },
};