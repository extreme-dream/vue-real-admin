import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import add from "./add.vue";
import edit from "./edit.vue";
export default {
  name: "index",
  components:{ActionButton,QueryForm,JmTable,add,edit},
    data() {
        return {
          compontedshowModal:"add",//弹窗组件
            showModal: false,//表单是否显示
            edit_id:10, 
            dataList:  [],
            ids:{},
            where:{
              email:"",
              mobile:"",
              username:"",
              status:"",
               
            },
            page:{
                total:500,
                pageSize:10,
                page:1
            },  
            columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   },
                    {label: "邮箱",  prop: "email",},
                    {label: "手机",  prop: "mobile",},
                    {label: "账号",      prop: "username",   }, 
                    {label: "昵称",      prop: "nickname",   }, 
                    {label: "头像",      prop: "avatar",  type:"avatar" ,}, 
                    {label: "组名",  prop: "groupname", },
                   
                      
                    {
                      label: "状态",
                      prop: "status",
                      width: 90,
                      formatter(row, column, value) {
                        return {
                          "1": "正常",
                          "-1": "停用",
                        }[value];
                      },
                    },
                    {label: "创建时间",  prop: "createtime",  
                        formatter(row, column, value) {
                          return utils.formateDate(new Date(value));
                        },},
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
      const res =   await this.$api.getAdmin({page:this.page.page,limit:this.page.pageSize});  
      this.dataList =res.rows;
      this.page.total=res.total
    },
       //分页的时候获取数据
    async  handleCurrentChange(val) {
   
        let list = await this.$api.getAdmin({page:val,limit:this.page.pageSize});
        this.dataList = list.rows;
        this.page.total=list.total; 
      },
      async del(_id){
    
        //  await this.$api.menuSubmit({ _id, action: "delete" });
        /*查看单条数据*/
        await this.$api.delAdmin({ids:_id}); 

          // console.log('id',_id);
         this.getDataList();
         
         this.$message.success("删除成功2");
      },
    // // 新增菜单
    // add(type, row) {
    //   this.showModal = true;
    //   this.action = "add";
      
		// 		this.$router.push('/admin/add')
    //   // if (type == 2) {
    //   //   this.menuForm.pid = [...row.pid, row._id].filter(
    //   //     (item) => item
    //   //   );
    //   // }
    // },
    // //修改
    // edit(row) {
    //      this.showModal = true;
    //   this.action = "edit";
    //   this.edit_id=row
		// 		this.$router.push('/admin/edit')
    //   // this.showModal = true;
    //   // this.action = "edit";
    //   // this.$nextTick(() => {
    //   //   Object.assign(this.menuForm, row);
    //   // });
    // },
     // 新增菜单
     add(type, row) {
          this.showModal = true;
          this.action = "add";
          this.compontedshowModal="add";
      },
      //修改
      edit(row) {
          this.showModal = true;
          this.action = "edit";
          this.edit_id=row
        this.compontedshowModal="edit";
      },
    //删除
    async alldel(_id) {
       
      console.log(this.ids);
      //   this.$message.success("删除成功all");
      
      var ids = "";
      for (let item of this.ids) {
        ids += item+ ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
   
      await this.$api.delAdmin({ids:ids}); 
       
      //   // console.log('id',_id);
       this.getDataList();
       
       this.$message.success("删除成功2");
   
    },
    setids(ids){
      this.ids=ids
  },
 
    async  handleSubmit(val) {
   
      let list = await this.$api.getAdmin({page:val,limit:this.page.pageSize,where:this.where});
      this.dataList = list.rows;
      this.page.total=list.total; 
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false; 
      this.handleReset("dialogForm");
    },
     closeDialog() {
      this.showModal = false;
      this.getDataList(); 
      this.handleReset("dialogForm");
    }
  },
};