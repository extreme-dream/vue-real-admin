import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue";
import JmTable from "../../../components/JmTable.vue";
import add from "./add.vue";
import edit from "./edit.vue";
 
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTable,add,edit},
  data() {
        return {
            compontedshowModal:"add",//弹窗组件
            showModal: false,//表单是否显示
            edit_id:10, //修改弹窗传值
           
            dataList:  [],//表格数据
            ids:[],//全选需要的ids
            columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   }, 
                    {label: "标题",  prop: "title",},
                    {label: "商品图标",      prop: "icon",  type:"icon" },  
                    {label: "接口",  prop: "apipath", },
                    {label: "前端路由",  prop: "name", },
                    {label: "父id",  prop: "pid", },
                    {label: "权限",  prop: "weigh", },
                    {
                      label: "菜单状态",
                      prop: "status",
                      width: 90,
                      formatter(row, column, value) {
                        return {
                          "1": "正常",
                          "-1": "停用",
                        }[value];
                      },
                    },  
                    {label: "创建时间",  prop: "create_time",},
                ],//行参数
        }
    }, 
  mounted() {
    //表格初始化
    this.getDataList();
  },
  methods: {
          // 菜单列表初始化
          async getDataList() { 
              const res =   await this.$api.getRule();  
              this.dataList =res.rows;
              this.page.total=res.total
          },
            /*删除*/
          async del(_id){
              await this.$api.delRule({ids:_id});  
              this.getDataList();
              this.$message.success("删除成功2");
          },
          // 新增菜单
          add(type, row) {
              this.showModal = true;
              this.action = "add";
              this.compontedshowModal="add";
          },
          //修改
          edit(row) {
              this.showModal = true;
              this.action = "edit";
              this.edit_id=row
            this.compontedshowModal="edit";
          },
          //全部删除
          async alldel(_id) {
            // 获取全选ids
              var ids = "";
              for (let item of this.ids) {
                ids += item+ ",";
              }
              //去掉最后一个逗号(如果不需要去掉，就不用写)
              if (ids.length > 0) {
                ids = ids.substr(0, ids.length - 1);
              }
              // 获取全选ids结束
              await this.$api.delRule({ids:ids});  
              //重新获取数据
              this.getDataList();
              this.$message.success("删除成功2");
        
          },
          // 菜单操作-提交
          async  handleSubmit(val) {
   
            let list = await this.$api.getRule({page:val,limit:this.page.pageSize,where:this.where});
            this.dataList = list.rows;
            this.page.total=list.total; 
          },
          //设置全选的ids选项
          setids(ids){
              this.ids=ids
          },
          // 弹框关闭
          handleClose() {
              this.showModal = false;
              this.getDataList(); 
              this.handleReset("dialogForm");
          },
    
  },
};