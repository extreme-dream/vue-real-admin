import utils from "../../../utils/utils";

import ActionButton from "../../../components/ActionButton.vue";
import QueryForm from "../../../components/QueryForm.vue"; 
import JmTb from "../../../components/JmTb.vue"; 

 
export default {
  name: "index",
  components:{ActionButton,QueryForm,JmTb},
    data() {
        return {
       
            showModal: false,//表单是否显示
            edit_id:10,
            extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                },
            dataList:  [],
            page:{
                total:{},
                pageSize:10,
                page:1
            },  
            columns: [  
                    {label: "管理员id",  prop: "id",},
                    {label: "管理员",  prop: "username",},
                    {label: "路由",      prop: "url",  }, 
                    {label: "标题",      prop: "title",  }, 
                    
                    {label: "创建时间",  prop: "createtime",  
                        formatter(row, column, value) {
                          return utils.formateDate(new Date(value));
                        },},
                ],//行参数
        }
    }, 
  mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
 
      const res =   await this.$api.admin_log({page:this.page.page,limit:this.page.pageSize});  
        this.dataList =res.rows;
        this.page.total=res.total
    },
       //分页的时候获取数据
     async handleCurrentChange(val) {
      const res =   await this.$api.admin_log({page:val,limit:this.page.pageSize});  
        this.dataList =res.rows;
        this.page.total=res.total
    },
    async handleSizeChange(val) {
      const res =   await this.$api.admin_log({page:this.page.page,limit:val});  
      this.dataList =res.rows;
      this.page.total=res.total
    },
    
    //删除
    async alldel(_id) {
        this.$message.success("删除成功all");
    //   await this.$api.menuSubmit({ _id, action: "delete" });
    //   this.$message.success("删除成功");
    //   this.getMenuList();
    },
    // 菜单操作-提交
    handleSubmit() {
      this.$refs.dialogForm.validate(async (valid) => {
        if (valid) {
          let { action, menuForm } = this;
          let params = { ...menuForm, action };
          let res = await this.$api.menuSubmit(params);
          this.showModal = false;
          this.$message.success("操作成功");
          this.handleReset("dialogForm");
          this.getMenuList();
        }
      });
    },
   
    
    },
};