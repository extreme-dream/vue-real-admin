import utils from "../../utils/utils";

import ActionButton from "../../components/ActionButton.vue";
import QueryForm from "../../components/QueryForm.vue";
import JmTable from "../../components/JmTable.vue";
import { getMockDataList } from './datalise';
export default {
  name: "menu",
  components:{ActionButton,QueryForm,JmTable},
    data() {
        return {
       
            showModal: false,//表单是否显示
            edit_id:10,
            extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                },
            dataList:  [],
            page:{
                total:500,
                pageSize:10,
              
            },  
            ids:[],
            columns: [ 
                    {label: "#",        prop: "id",  width: 150, type:"selection"   }, 
                   
                
                  
                    // status: 1, 
                    // intro: '这是一种水果，这是一种水果，这是一种水果，这是一种水果，这是一种水果，这是一种水果，这是一种水果，这是一种水果',
                    // img: 'http://sa-admin.dev33.cn/sa-frame/admin-logo.png',
                    // audio: 'http://demo-jj.dev33.cn/spdj-server/upload/in-file/15998054742041893440109.mp3',
                    // video: 'http://demo-jj.dev33.cn/spdj-server/upload/in-file/1599805482152468147415.mp4',
                    // file: 'http://demo-jj.dev33.cn/spdj-server/upload/in-file/1599805482152468147415.mp4',
                    // textList: '春眠不觉晓,处处闻啼鸟',
                    // create_time: new Date()
                    {label: "名称",        prop: "name",    }, 
                    {label: "icon",        prop: "icon",  type:"icon"   }, 
                    {label: "avatar",        prop: "avatar",    type:"avatar"   }, 
                    {label: "count",        prop: "count",   type:"tc-num"   }, 
                    {label: "money",        prop: "money",   type:"money"   }, 
                    {label: "money",        prop: "money",   type:"money-f"   }, 
                    {label: "rate",        prop: "rate",   type:"rate"   },  
                    {label: "intro",        prop: "intro",   type:"textarea"   }, 
                    {label: "intro",        prop: "intro",   type:"rich-text"   }, 
                    {label: "avatar",        prop: "avatar",   type:"link"   },  
                    {label: "avatar",        prop: "avatar",   type:"img"   }, 
                    {label: "create_time",        prop: "create_time",   type:"date"   }, 
                    {label: "datetime",        prop: "create_time",   type:"datetime"   },  
                    
                ],//行参数
        }
    }, 
    mounted() {
    this.getDataList();
  },
  methods: {
    // 菜单列表初始化
    async getDataList() { 
        let dataList= getMockDataList(); 
        this.dataList =dataList.data;
        
    },
     
      async del(_id){ 
            console.log('id',_id); 
        },
    // 新增弹窗 
    add() {
      //
      this.showModal = true;
      this.action = "add";
       this.$router.push('/rule/add')
  
    },
    //修改弹窗
    edit(row) {
        this.showModal = true;
        this.action = "edit";
        this.edit_id=row 
				this.$router.push('/rule/edit')
       
    },
    
    //删除
    async alldel(_id) {
       
      console.log(this.ids);
      //   this.$message.success("删除成功all");
      
      var ids = "";
      for (let item of this.ids) {
        ids += item+ ",";
      }
      //去掉最后一个逗号(如果不需要去掉，就不用写)
      if (ids.length > 0) {
        ids = ids.substr(0, ids.length - 1);
      }
   
      await this.$api.delRule({ids:ids}); 
       
      //   // console.log('id',_id);
       this.getDataList();
       
       this.$message.success("删除成功2");
   
    },
    // 菜单操作-提交
    async handleSubmit() {
      this.$refs.dialogForm.validate(async (valid) => {
        if (valid) {
          let { action, menuForm } = this;
          let params = { ...menuForm, action };
          let res = await this.$api.menuSubmit(params);
          this.showModal = false;
          this.$message.success("操作成功");
          this.handleReset("dialogForm");
          this.getMenuList();
        }
      });
    },
    setids(ids){
        this.ids=ids
    },
    // 弹框关闭
    handleClose() {
      this.showModal = false;
      this.getDataList();
      this.$router.push('/rule')
      this.handleReset("dialogForm");
    },
  },
};