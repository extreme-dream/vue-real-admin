/**
 * 环境配置封装
 */
const env = import.meta.env.MODE ||'prod'||"build";
const EnvConfig = {
    dev: {
        baseApi: '/api',
        mockApi:'http://real-think.jmwl51.com/',
    },
    test: {
        baseApi: '/api',
        mockApi:'http://real-think.jmwl51.com/',
    },
    build: {
        baseApi: '/api',
        mockApi:'http://real-think.jmwl51.com/',
    },
    production: {
        baseApi: '/api',
        mockApi:'http://real-think.jmwl51.com/',
    },
    prod: {
        baseApi: '/api',
        mockApi:'http://real-think.jmwl51.com/',
    },
}

export default {
    env,
    namespace:'czlManger',
    mock: true,
    ...EnvConfig[env]
}